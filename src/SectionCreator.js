import { addJoinUsSection, removeJoinUsSection } from './join-us-section';
import {
  JOIN_PROGRAM_SECTION_HEADER,
  JOIN_PROGRAM_SECTION_HEADER_ADVANCED,
  JOIN_PROGRAM_SECTION_BUTTON_TEXT,
  JOIN_PROGRAM_SECTION_BUTTON_TEXT_ADVANCED
} from '/src/constants/text-constants';

export class SectionCreator{
  create(type) {
    switch (type) {
      case 'advanced':
        return new Section(JOIN_PROGRAM_SECTION_HEADER_ADVANCED, JOIN_PROGRAM_SECTION_BUTTON_TEXT_ADVANCED);
      case 'standard':
      default:
        return new Section(JOIN_PROGRAM_SECTION_HEADER, JOIN_PROGRAM_SECTION_BUTTON_TEXT);
    }
  }
}

class Section {
  constructor(headerText, buttonText) {
    this.headerText = headerText;
    this.buttonText = buttonText;
  }

  render() {
    addJoinUsSection(this.headerText, this.buttonText);
  }

  remove() {
    removeJoinUsSection();
  }
}
