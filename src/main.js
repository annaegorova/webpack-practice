import { SectionCreator } from './SectionCreator';
import '/src/styles/style.css'

document.addEventListener('DOMContentLoaded', () => {
  new SectionCreator().create('standard').render();
});
