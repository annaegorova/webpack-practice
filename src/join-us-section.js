import { validate } from '/src/email-validator';
import { APP_SUBTITLE_TEXT, JOIN_PROGRAM_SECTION_EMAIL_PLACEHOLDER } from '/src/constants/text-constants'
import {
  APP_TITLE,
  APP_SUBTITLE,
  APP_CONTAINER,
  APP_SECTION,
  APP_SECTION_BUTTON,
  APP_FOOTER,
  JOIN_PROGRAM_SECTION,
  JOIN_PROGRAM_SECTION_BUTTON,
  JOIN_PROGRAM_SECTION_EMAIL,
  JOIN_PROGRAM_SECTION_FORM
} from '/src/constants/selector-constants'

export function addJoinUsSection(headerText, buttonText) {

  const section = createElement('section', [APP_SECTION, JOIN_PROGRAM_SECTION]);
  addNewElement(section, 'h2', APP_TITLE, headerText);
  addNewElement(section, 'h3', APP_SUBTITLE, APP_SUBTITLE_TEXT);

  section.appendChild(createJoinUsSectionForm(buttonText));

  const app = document.getElementById(APP_CONTAINER);
  const footer = document.getElementsByClassName(APP_FOOTER)[0];
  app.insertBefore(section, footer);
}

export function removeJoinUsSection() {
  const section = document.getElementsByClassName(JOIN_PROGRAM_SECTION)[0];
  section.parentNode.removeChild(section);
}

function createJoinUsSectionForm(submitButtonText) {
  const form = createElement('form', JOIN_PROGRAM_SECTION_FORM);
  const input = createElement('input', JOIN_PROGRAM_SECTION_EMAIL);
  input.setAttribute('type', 'text');
  input.setAttribute('placeholder', JOIN_PROGRAM_SECTION_EMAIL_PLACEHOLDER);
  form.appendChild(input);
  addNewElement(form, 'button', [APP_SECTION_BUTTON, JOIN_PROGRAM_SECTION_BUTTON], submitButtonText);

  form.addEventListener('submit', (e) => {
    e.preventDefault();
    const email  =  document.querySelector('.join-program-section__email').value;
    console.log(email);
    alert(validate(email));
  });

  return form;

}

function createElement(elemType, classNames, innerText) {
  const elem = document.createElement(elemType);

  Array.isArray(classNames) ? elem.classList.add(...classNames) : elem.classList.add(classNames);

  if (innerText) {
    elem.innerText = innerText;
  }

  return elem;
}

function addNewElement(parentElem, elemType, classNames, innerText) {
  parentElem.appendChild(createElement(elemType, classNames, innerText));
}
