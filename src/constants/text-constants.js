export const APP_SUBTITLE_TEXT = 'Sed do eiusmod tempor incididunt \nut labore et dolore magna aliqua.';

export const JOIN_PROGRAM_SECTION_HEADER = 'Join Our Program';
export const JOIN_PROGRAM_SECTION_HEADER_ADVANCED = 'Join Our Advanced Program';
export const JOIN_PROGRAM_SECTION_EMAIL_PLACEHOLDER = 'Email';
export const JOIN_PROGRAM_SECTION_BUTTON_TEXT = 'Subscribe';
export const JOIN_PROGRAM_SECTION_BUTTON_TEXT_ADVANCED = 'Subscribe to Advanced Program';
